mov ax, 4 # Operand 1
mov bx, 2 # Operand 2

mov dx, ax
mov cx, 0
add dx, bx
store cx, dx # Result of addition stored at 0x0

mov dx, ax
mov cx, 1
sub dx, bx
store cx, dx # Result of addition stored at 0x1

mov dx, ax
mov cx, 2
mult dx, bx
store cx, dx # Result of addition stored at 0x2

mov dx, ax
mov cx, 3
div dx, bx
store cx, dx # Result of addition stored at 0x3
halt
