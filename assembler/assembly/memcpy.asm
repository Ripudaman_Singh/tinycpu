# Program to copy contents of one memory location to another

mov dx, 5 # Store some contents before this 0x5
loop1:
	sub dx, 1
	jmpn endloop1

	store dx, dx

    xor cx, cx
	jmpz loop1
endloop1:

mov dx, 5 # Total number of words to copy
loop2:
	sub dx, 1
	jmpn endloop2

	mov bx, 0 # Starting address of contents to be copied
	add bx, dx
	load ax, bx

	mov bx, 10 # Starting address of destination
	add bx, dx
	store bx, ax

	xor cx, cx
	jmpz loop2

endloop2:
halt
